const express = require('express')
const app = express()
const path = require('path')

app.use('/', express.static(path.join(__dirname, 'dist')))

app.listen(80, function () {
  console.log('Tacky started')
})
