const uuid = require('uuid/v1')

function buildStack (options) {
  const stack = {
    id: uuid(),
    name: null,
    parent: null,
    children: [],
    siblings: {
      left: null,
      right: null
    },
    content: '',
    state: '',
    focused: true,
    highlight: false
  }

  if (options) {
    for (let key in options) {
      stack[key] = options[key]
    }

    if (options.expand) {
      stack.children = stack.children.map((c) => buildStack({...c, parent: stack.id, expand: true}))
    }
  }

  return stack
}

export default buildStack
