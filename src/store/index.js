import Vue from 'vue'
import Vuex from 'vuex'
import buildStack from '../stack.js'
import cloneDeep from 'clone-deep'
import Data from '../services/data.js'
// let d = Data.mock()
// let tree = new Stack({...d, expand: true})
const tree = buildStack(null)
const context = tree.id
const state = {
  nodes: {
    [context]: tree
  },
  tree,
  context,
  mode: 'Insert',
  dragging: false
}
let states = [state]
let statesAhead = []
let registers = {
  drag: null,
  copy: null
}

Vue.use(Vuex)

const copy = (state) => {
  const tree = cloneDeep(state.tree)
  const nodes = Data.flatten(tree)
  return {
    nodes,
    tree,
    context: state.context,
    mode: state.mode,
    dragging: false
  }
}

const addChild = (state, data) => {
  let parent = state.nodes[data.id]
  let children = parent.children
  let latestChild = parent.children.length ? parent.children[parent.children.length - 1] : null
  let child = data.child || buildStack({
    parent: parent.id
  })

  if (latestChild) {
    child.siblings.left = latestChild.id
    latestChild.siblings.right = child.id
  }
  state.nodes[child.id] = child
  children.push(child)
}

const putRegister = (register, data) => {
  registers[register] = data
}

export default new Vuex.Store({
  state,
  mutations: {
    FOCUS (state, id) {
      state.nodes[id].focused = true

      for (let n in state.nodes) {
        state.nodes[n].highlight = false
      }

      const highlight = (node) => {
        node.highlight = true
        node.children.forEach((child) => highlight(child))
      }

      highlight(state.nodes[id])
    },
    ZOOM (state, e) {
      state.context = e
    },
    UP (state) {
      state.context = state.nodes[state.context].parent
    },
    RESET (state) {
      state.context = state.tree.id
    },
    CLEAR (state) {
      states.push(copy(state))
      state.nodes[tree] = buildStack(null)
      state.context = state.tree.id
    },
    ADD_CHILD (state, data) {
      if (!data.stateless) {
        states.push(copy(state))
      }
      addChild(state, data)
    },
    APPEND (state, data) {
      let register = registers[data.register]
      if (register && register !== data.id) {
        let child = cloneDeep(state.nodes[register])

        // Recurse over the children to generate new nodes
        const addChildren = (child, parent) => {
          delete child.id
          delete child.siblings.left
          delete child.siblings.right
          let newChild = buildStack({...child, parent})

          addChild(state, { id: parent, child: newChild, stateless: true })

          for (let c in newChild.children) {
            addChildren(newChild.children[c], newChild.id)
            newChild.children.splice(c, 1)
          }
        }

        addChildren(child, data.id)
      }
    },
    DELETE (state, id) {
      states.push(copy(state))
      let parent = state.nodes[state.nodes[id].parent]
      parent.children = parent.children.filter((c) => { return c.id !== id })
      delete state.nodes[id]
    },
    LOAD (state, d) {
      states.push(copy(state))
      state.tree = buildStack({...d, expand: true})
      state.nodes = Data.flatten(state.tree)
      state.context = state.tree.id
    },
    CHANGE_MODE (state, mode) {
      state.mode = mode
    },
    MARK_COMPLETE (state, id) {
      states.push(copy(state))
      const markComplete = (node) => {
        node.state = 'Complete'
        node.children.forEach((child) => markComplete(child))
      }

      markComplete(state.nodes[id])
    },
    MARK_INCOMPLETE (state, id) {
      states.push(copy(state))
      const markIncomplete = (node) => {
        node.state = 'Incomplete'
        if (node.parent) {
          markIncomplete(state.nodes[node.parent])
        }
      }

      markIncomplete(state.nodes[id])
    },
    UPDATE_NAME (state, data) {
      state.nodes[data.id].name = data.name
    },
    START_DRAG (state) {
      state.dragging = true
    },
    END_DRAG (state) {
      state.dragging = false
    },
    UNDO (state) {
      if (states.length) {
        statesAhead.push(copy(state))
        Object.assign(state, states[states.length - 1])
        states = states.slice(0, states.length - 1)
      }
    },
    REDO (state) {
      if (statesAhead.length) {
        states.push(copy(state))
        Object.assign(state, statesAhead[statesAhead.length - 1])
        statesAhead = statesAhead.slice(0, statesAhead.length - 1)
      }
    }
  },
  actions: {
    copy ({ commit }, id) {
      putRegister('copy', id)
    },
    paste ({ commit }, id) {
      commit('APPEND', { register: 'copy', id })
    },
    drag ({ commit }, id) {
      putRegister('drag', id)
      commit('START_DRAG')
    },
    drop ({ commit }, id) {
      commit('APPEND', { register: 'drag', id })
      putRegister('drag', null)
      commit('END_DRAG')
    },
    addChild ({ commit }, id) {
      commit('ADD_CHILD', { id })
    }
  }
})
