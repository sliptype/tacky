const Help = {
  'Insert Mode': [
    {
      action: 'Enter command mode',
      command: 'Escape'
    },
    {
      action: 'Add a layer',
      command: 'Enter'
    },
    {
      action: 'Add a case',
      command: 'Shift + Enter'
    }
  ],
  'Command Mode': [
    {
      action: 'Enter insert mode',
      command: 'Enter'
    },
    {
      action: 'Delete a case',
      command: 'Shift + Delete'
    },
    {
      action: 'Copy cases',
      command: 'Drag'
    },
    {
      action: 'Zoom to a case',
      command: '+'
    },
    {
      action: 'Zoom out one level',
      command: '-'
    },
    {
      action: 'Reset zoom',
      command: '0'
    }
  ],
  'Global': [
    {
      action: 'Undo',
      command: 'Control + Z'
    },
    {
      action: 'Redo',
      command: 'Control + Y'
    }
  ]
}

export default Help
