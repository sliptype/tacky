export default (function () {
  const traverse = function (tree, basket = [], base = [], id, level = 0) {
    if (!tree.children.length) {
      if (!tree.content) {
        basket.push({ id: id || tree.id, path: base })
      } else {
        basket.push({ id: id || tree.id, path: [...base, tree.content] })
      }
    } else {
      for (let i in tree.children) {
        traverse(tree.children[i], basket, [...base, tree.content], level === 1 ? tree.id : id, level + 1)
      }
    }
    return basket
  }

  const flatten = (tree, result = {}) => {
    result[tree.id] = tree
    for (let c in tree.children) {
      flatten(tree.children[c], result)
    }
    return result
  }
  return {
    traverse,
    flatten,
    mock () {
      return mockData
    }
  }
})()

const mockData = {'id': '48e3c9a0-9428-11e7-9cd8-af90d058e6a9', 'child': [{'id': '4a90dd60-9428-11e7-9cd8-af90d058e6a9', 'child': null, 'content': 'ok'}, {'id': '4b693c00-9428-11e7-9cd8-af90d058e6a9', 'child': [{'id': '4c03a740-9428-11e7-9cd8-af90d058e6a9', 'child': null, 'content': ''}], 'content': 'ok'}], 'content': 'test'}

