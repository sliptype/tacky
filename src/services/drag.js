import {EventBus} from '../bus.js'

export default (function () {
  let dragData = null

  const drag = function (data) {
    dragData = data
    EventBus.$emit('dragStart')
  }

  const drop = function () {
    dragData = null
    EventBus.$emit('dragEnd')
  }

  const getDragData = () => dragData

  return {
    drag,
    drop,
    getDragData
  }
})()
